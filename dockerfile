FROM balenalib/raspberrypi3-debian-python:3.8

WORKDIR /usr/src/app

# environment variables
ENV BLUEZ_VERSION 5.54

RUN apt-get update && apt-get install -y \
    build-essential git wget make libical-dev libdbus-1-dev libglib2.0-dev libreadline-dev libudev-dev systemd pkg-config libcairo2-dev gcc python3-dev libgirepository1.0-dev

RUN wget -P /tmp/ https://www.kernel.org/pub/linux/bluetooth/bluez-${BLUEZ_VERSION}.tar.gz \
 && tar xf /tmp/bluez-${BLUEZ_VERSION}.tar.gz -C /tmp \
 && cd /tmp/bluez-${BLUEZ_VERSION} \
 && ./configure --prefix=/usr \
    --mandir=/usr/share/man \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --enable-library \
    --enable-experimental \
    --enable-maintainer-mode \
    --enable-deprecated \
 && make \
 && make install

RUN cp /tmp/bluez-${BLUEZ_VERSION}/attrib/gatttool /usr/local/bin/

RUN pip3 install bluepy pylgbst dbus-python gatt gobject PyGObject \
 && git clone https://github.com/undera/pylgbst.git

# COPY demo.py ./
COPY start.sh ./

RUN chmod +x ./start.sh

CMD ["./start.sh"]

# docker build . -t rpibluepy
# docker run --privileged -v "/var/run/dbus/system_bus_socket:/var/run/dbus/system_bus_socket" -it --net host rpibluepy