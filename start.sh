#!/bin/bash

echo "[INFO] devices:"
ls -l /sys/class/bluetooth
hcitool dev

echo "[INFO] 10sec scan for Move Hub"
mh=$(timeout -s INT 10s hcitool lescan | grep 'Move Hub')
mhmac=$(echo $mh | tr -d ' Move Hub')

echo "[INFO] found: ${mhmac}"

# echo "starting gatt tool" "execute commands: " "connect, primary, characteristics" "get device id with: " "char-read-hnd 0x07" "set LED to yellow with: " "char-write-cmd 0x0e 0800813211510008"
# /usr/local/bin/gatttool -b ${mhmac} -I
# be fast to set the led to yellow, it times out very quick otherwise and you need to reconnect

echo  "start python demo"
python3 /usr/src/app/pylgbst/examples/demo.py -c "gatt://${mhmac}"
# python3 /usr/src/app/demo.py -c "gatt://${mhmac}"
